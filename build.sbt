import Dependencies._

ThisBuild / scalaVersion := "2.13.9"
ThisBuild / organization := "ch.swisscollections"
ThisBuild / organizationName := "swisscollections"
ThisBuild / git.gitTagToVersionNumber := { tag: String =>
  if (tag matches "[0-9]+\\..*") {
    Some(tag)
  }
  else {
    None
  }
}



lazy val root = (project in file("."))
  .enablePlugins(GitVersioning)
  .settings(
    name := "Enricher",
    assemblyJarName in assembly := "app.jar",
    test in assembly := {},
    assemblyMergeStrategy in assembly := {
      case "log4j.properties" => MergeStrategy.first
      case "log4j2.xml" => MergeStrategy.first
      case other if other.contains("module-info.class") => MergeStrategy.discard
      //https://stackoverflow.com/questions/54625572/sbt-assembly-errordeduplicate-different-file-contents-found-in-io-netty-versio
      //todo: study the mechanisms!
      case "META-INF/io.netty.versions.properties" => MergeStrategy.concat

      case PathList("org", "apache", "commons", "logging", "Log.class") => MergeStrategy.last
      case PathList("org", "apache", "commons", "logging", "LogConfigurationException.class") => MergeStrategy.last
      case PathList("org", "apache", "commons", "logging", "LogFactory.class") => MergeStrategy.last
      case PathList("org", "apache", "commons", "logging", "impl", "NoOpLog.class") => MergeStrategy.last
      case PathList("org", "apache", "commons", "logging", "impl", "NoOpLog.class") => MergeStrategy.last
      case PathList("org", "apache", "commons", "logging", "impl", "SimpleLog$1.class") => MergeStrategy.last
      case PathList("org", "apache", "commons", "logging", "impl", "SimpleLog.class") => MergeStrategy.last


      case x =>
        val oldStrategy = (assemblyMergeStrategy in assembly).value
        oldStrategy(x)
    },
    mainClass in assembly := Some("ch.swisscollections.App"),
    resolvers ++= Seq(
      "Memobase Libraries" at "https://gitlab.switch.ch/api/v4/projects/1324/packages/maven"
    ),
    libraryDependencies ++= Seq(
      jodaTime,
      kafkaClients,
      kafkaStreams,
      kafkaStreamsTestUtils,
      log4jApi,
      log4jCore,
      log4jSlf4j,
      log4jScala,
      memobaseServiceUtils
        excludeAll(
        ExclusionRule(organization = "org.slf4j", name = "slf4j-api"),
        ExclusionRule(organization = "org.slf4j", name = "slf4j-log4j12"),
        ExclusionRule(organization = "org.slf4j", name = "jcl-over-slf4j"),
      ),
      scalatic,
      scalaTest % Test,
      scala_xml,
      saxon,
      snakeyaml,
      solrj,
      mongoclient
    )
  )

