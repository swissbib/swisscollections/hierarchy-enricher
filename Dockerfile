FROM eclipse-temurin:8-jre
ADD target/scala-2.13/app.jar /app/app.jar
ADD src/main/resources/app.yml /configs/app.yml
CMD java -jar /app/app.jar
