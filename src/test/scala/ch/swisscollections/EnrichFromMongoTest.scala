/*
 * media-metadata-extractor
 * Copyright (C) 2020  Memoriav
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package ch.swisscollections

import ch.swisscollections.KafkaTopologyUtils._
import org.scalatest.Tag
import org.scalatest.funsuite.AnyFunSuite

import scala.io.Source
import scala.xml.{Elem, XML}
import EnrichFromMongo._
import org.scalactic.Explicitly.after
import org.scalatest.StreamlinedXml.streamlined
import org.scalatest.matchers.should.Matchers.equal
import org.scalatest.matchers.should.Matchers.{convertToAnyShouldWrapper, equal}


class EnrichFromMongoTest extends AnyFunSuite {

  private def getTextFromResourcesFolder(filepath: String): String = {
    val file = Source.fromFile(s"src/test/resources/$filepath")
    val text = file.mkString
    file.close()
    text
  }

  private def getXmlElemFromResourcesFolder(filepath: String): Elem = {
    val file = Source.fromFile(s"src/test/resources/$filepath")
    val text = file.mkString
    val xmlElement = XML.loadString(text)
    file.close()
    xmlElement
  }

  test("Test Get VLID") {
    val xmlElement = getXmlElemFromResourcesFolder("record/mongo-erara.xml")
    val vlid = getIdDigitalPlatform(xmlElement).getOrElse("")
    assert(vlid == "2940903")
  }

  test("Test Get platform") {
    val xmlElement = getXmlElemFromResourcesFolder("record/mongo-erara.xml")
    val vlid = getDigitalPlatform(xmlElement).getOrElse("")
    assert(vlid == "e-rara.ch")
  }

  test("Test Get IIIF manifest url") {
    val iiifUrl = buildIiifManifestUrl("e-rara.ch", "2940903").getOrElse("")
    assert(iiifUrl == "https://www.e-rara.ch/i3f/v20/2940903/manifest")

    val iiifUrl2 = buildIiifManifestUrl("e-manuscripta.ch", "3985380").getOrElse("")
    assert(iiifUrl2 == "https://www.e-manuscripta.ch/i3f/v20/3985380/manifest")
  }

  test("Test Get doi from solr Record") {
    val solrRecord = getXmlElemFromResourcesFolder("record/solrdoc-erara.xml")
    val doi = getEplatformDoi(solrRecord)
    assert(doi.get == "10.3931/e-rara-10016")

    val solrRecord2 = getXmlElemFromResourcesFolder("record/solrdoc.xml")
    val doi2 = getEplatformDoi(solrRecord2)
    assert(doi2 == None)
  }

  test("Get licence") {
    val xmlElement = getXmlElemFromResourcesFolder("record/mongo-erara.xml")
    val licence = getDigitalReproductionLicence(xmlElement).getOrElse("no-licence")

    assert(licence == "pdm")
  }

  test("Get owner") {
    val xmlElement = getXmlElemFromResourcesFolder("record/mongo-erara.xml")
    val owner = getDigitalReproductionRightOwner(xmlElement).getOrElse("no-owner")

    assert(owner == "Zentralbibliothek Zürich")
  }

  test("Get call number") {
    val xmlElement = getXmlElemFromResourcesFolder("record/mongo-erara-2.xml")
    val callNumber = getDigitalReproductionCallNumber(xmlElement).getOrElse("no-call-number")

    assert(callNumber == "M&P 3: 246")

    val xmlElement2 = getXmlElemFromResourcesFolder("record/mongo-erara.xml")
    val callNumber2 = getDigitalReproductionCallNumber(xmlElement2).getOrElse("no-call-number")

    assert(callNumber2 == "6.134,9")
  }

  test("Get digital object page details") {
    val xmlElement = getXmlElemFromResourcesFolder("record/mongo-erara.xml")
    val pageDetails = getDigitalObjectPagesDetails(xmlElement)
    val expected = List("2940906###Scan 2 &amp; irgendwas###1###0", "2940907###Scan 3###2###0", "2940908###Scan 4###3###0", "2940909###Scan 5###4###0", "2940910###Scan 6###5###0", "2940911###Scan 7###6###0", "2940912###Scan 8###7###0", "2940913###Scan 9###8###0")

    assert(pageDetails == expected)

    val xmlElement2 = getXmlElemFromResourcesFolder("record/mongo-erara-3.xml")
    val pageDetails2 = getDigitalObjectPagesDetails(xmlElement2)
    val expected2 = List("14257006###[Seite 2]###1###1", "14257007###[Seite 3]###2###1", "14257008###[Seite 4]###3###1", "14257009###[Seite 5]###4###1", "14257010###[Seite 6]###5###1", "14257011###[Seite 7]###6###1", "14257012###[Seite 8]###7###1", "14257013###[Seite 9]###8###1", "14257014###[Seite 10]###9###1", "14257015###[Seite 11]###10###1", "14257016###[Seite 12]###11###1", "14257017###[Seite 13]###12###1", "14257018###[Seite 14]###13###1", "14257019###[Seite 15]###14###1", "14257020###[Seite 16]###15###1", "14257021###[Seite 17]###16###1", "14257022###[Seite 18]###17###1", "14257023###[Seite 19]###18###1", "14257024###[Seite 20]###19###1", "14257025###[Seite 21]###20###1", "14257026###[Seite 22]###21###1", "14257027###[Seite 23]###22###1", "14257028###[Seite 24]###23###1", "14257029###[Seite 25]###24###1", "14257030###[Seite 26]###25###1", "14257031###[Seite 27]###26###1", "14257032###[Seite 28]###27###1", "14257033###[Seite 29]###28###1", "14257034###[Seite 30]###29###1", "14257035###[Seite 31]###30###1", "14257036###[Seite 32]###31###1", "14257037###[Seite 33]###32###1", "14257038###[Seite 34]###33###1", "14257039###[Seite 35]###34###1", "14257040###[Seite 36]###35###1", "14257041###[Seite 37]###36###1", "14257042###[Seite 38]###37###1", "14257043###[Seite 39]###38###1", "14257044###[Seite 40]###39###1", "14257045###[Seite 41]###40###1", "14257046###[Seite 42]###41###1", "14257047###[Seite 43]###42###1", "14257048###[Seite 44]###43###1", "14257049###[Seite 45]###44###1", "14257050###[Seite 46]###45###1", "14257051###[Seite 47]###46###1", "14257052###[Seite 48]###47###1", "14257053###[Seite 49]###48###1", "14257054###[Seite 50]###49###1", "14257055###[Seite 51]###50###1", "14257056###[Seite 52]###51###1", "14257057###[Seite 53]###52###1", "14257058###[Seite 54]###53###1", "14257059###[Seite 55]###54###1", "14257060###[Seite 56]###55###1", "14257061###[Seite 57]###56###1", "14257062###[Seite 58]###57###1", "14257063###[Seite 59]###58###1", "14257064###[Seite 60]###59###1", "14257065###[Seite 61]###60###1", "14257066###[Seite 62]###61###1", "14257067###[Seite 63]###62###1", "14257068###[Seite 64]###63###1", "14257069###[Seite 65]###64###1", "14257070###[Seite 66]###65###1", "14257071###[Seite 67]###66###1", "14257072###[Seite 68]###67###1", "14257073###[Seite 69]###68###1", "14257074###[Seite 70]###69###1", "14257075###[Seite 71]###70###1", "14257076###[Seite 72]###71###1", "14257077###[Seite 73]###72###1", "14257078###[Seite 74]###73###1", "14257079###[Seite 75]###74###1", "14257080###[Seite 76]###75###1", "14257081###[Seite 77]###76###1", "14257082###[Seite 78]###77###1", "14257083###[Seite 79]###78###1", "14257084###[Seite 80]###79###1", "14257085###[Seite 81]###80###1", "14257086###[Seite 82]###81###1", "14257087###[Seite 83]###82###1", "14257088###[Seite 84]###83###1", "14257089###[Seite 85]###84###1", "14257090###[Seite 86]###85###1", "14257091###[Seite 87]###86###1", "14257092###[Seite 88]###87###1", "14257093###[Seite 89]###88###1", "14257094###[Seite 90]###89###1", "14257095###[Seite 91]###90###1", "14257096###[Seite 92]###91###1", "14257097###[Seite 93]###92###1", "14257098###[Seite 94]###93###1", "14257099###[Seite 95]###94###1", "14257100###[Seite 96]###95###1", "14257101###[Seite 97]###96###1", "14257102###[Seite 98]###97###1", "14257103###[Seite 99]###98###1", "14257104###[Seite 100]###99###1", "14257105###[Seite 101]###100###1", "14257106###[Seite 102]###101###1", "14257107###[Seite 103]###102###1", "14257108###[Seite 104]###103###1", "14257109###[Seite 105]###104###1", "14257110###[Seite 106]###105###1", "14257111###[Seite 107]###106###1", "14257112###[Seite 108]###107###1", "14257113###[Seite 109]###108###1", "14257114###[Seite 110]###109###1", "14257115###[Seite 111]###110###1", "14257116###[Seite 112]###111###1", "14257117###[Seite 113]###112###1", "14257118###[Seite 114]###113###1", "14257119###[Seite 115]###114###1", "14257120###[Seite 116]###115###1", "14257121###[Seite 117]###116###1", "14257122###[Seite 118]###117###1", "14257123###[Seite 119]###118###1", "14257124###[Seite 120]###119###1", "14257125###[Seite 121]###120###1", "14257126###[Seite 122]###121###1", "14257127###[Seite 123]###122###1", "14257128###[Seite 124]###123###1", "14257129###[Seite 125]###124###1", "14257130###[Seite 126]###125###1", "14257131###[Seite 127]###126###1", "14257132###[Seite 128]###127###1", "14257133###[Seite 129]###128###1", "14257134###[Seite 130]###129###1", "14257135###[Seite 131]###130###1", "14257136###[Seite 132]###131###1", "14257137###[Seite 133]###132###1", "14257138###[Seite 134]###133###1", "14257139###[Seite 135]###134###1", "14257140###[Seite 136]###135###1", "14257141###[Seite 137]###136###1", "14257142###[Seite 138]###137###1", "14257143###[Seite 139]###138###1", "14257144###[Seite 140]###139###1", "14257145###[Seite 141]###140###1", "14257146###[Seite 142]###141###1", "14257147###[Seite 143]###142###1", "14257148###[Seite 144]###143###1", "14257149###[Seite 145]###144###1", "14257150###[Seite 146]###145###1", "14257151###[Seite 147]###146###1", "14257152###[Seite 148]###147###1", "14257153###[Seite 149]###148###1", "14257154###[Seite 150]###149###1", "14257155###[Seite 151]###150###1", "14257156###[Seite 152]###151###1", "14257157###[Seite 153]###152###1", "14257158###[Seite 154]###153###1", "14257159###[Seite 155]###154###1", "14257160###[Seite 156]###155###1", "14257161###[Seite 157]###156###1", "14257162###[Seite 158]###157###1", "14257163###[Seite 159]###158###1", "14257164###[Seite 160]###159###1", "14257165###[Seite 161]###160###1", "14257166###[Seite 162]###161###1", "14257167###[Seite 163]###162###1", "14257168###[Seite 164]###163###1", "14257169###[Seite 165]###164###1", "14257170###[Seite 166]###165###1", "14257171###[Seite 167]###166###1", "14257172###[Seite 168]###167###1", "14257173###[Seite 169]###168###1", "14257174###[Seite 170]###169###1", "14257175###[Seite 171]###170###1", "14257176###[Seite 172]###171###1", "14257177###[Seite 173]###172###1", "14257178###[Seite 174]###173###1", "14257179###[Seite 175]###174###1", "14257180###[Seite 176]###175###1", "14257181###[Seite 177]###176###1", "14257182###[Seite 178]###177###1", "14257183###[Seite 179]###178###1", "14257184###[Seite 180]###179###1", "14257185###[Seite 181]###180###1", "14257186###[Seite 182]###181###1", "14257187###[Seite 183]###182###1", "14257188###[Seite 184]###183###1", "14257189###[Seite 185]###184###1", "14257190###[Seite 186]###185###1", "14257191###[Seite 187]###186###1", "14257192###[Seite 188]###187###1", "14257193###[Seite 189]###188###1", "14257194###[Seite 190]###189###1", "14257195###[Seite 191]###190###1", "14257196###[Seite 192]###191###1", "14257197###[Seite 193]###192###1", "14257198###[Seite 194]###193###1", "14257199###[Seite 195]###194###1", "14257200###[Seite 196]###195###1", "14257201###[Seite 197]###196###1", "14257202###[Seite 198]###197###1", "14257203###[Seite 199]###198###1", "14257204###[Seite 200]###199###1", "14257205###[Seite 201]###200###1", "14257206###[Seite 202]###201###1", "14257207###[Seite 203]###202###1", "14257208###[Seite 204]###203###1", "14257209###[Seite 205]###204###1", "14257210###[Seite 206]###205###1", "14257211###[Seite 207]###206###1", "14257212###[Seite 208]###207###1", "14257213###[Seite 209]###208###1", "14257214###[Seite 210]###209###1", "14257215###[Seite 211]###210###1", "14257216###[Seite 212]###211###1", "14257217###[Seite 213]###212###1", "14257218###[Seite 214]###213###1", "14257219###[Seite 215]###214###1", "14257220###[Seite 216]###215###1", "14257221###[Seite 217]###216###1", "14257222###[Seite 218]###217###1", "14257223###[Seite 219]###218###1", "14257224###[Seite 220]###219###1", "14257225###[Seite 221]###220###1", "14257226###[Seite 222]###221###1", "14257227###[Seite 223]###222###1", "14257228###[Seite 224]###223###1", "14257229###[Seite 225]###224###1", "14257230###[Seite 226]###225###1", "14257231###[Seite 227]###226###1", "14257232###[Seite 228]###227###1", "14257233###[Seite 229]###228###1", "14257234###[Seite 230]###229###1", "14257235###[Seite 231]###230###1", "14257236###[Seite 232]###231###1", "14257237###[Seite 233]###232###1", "14257238###[Seite 234]###233###1", "14257239###[Seite 235]###234###1", "14257240###[Seite 236]###235###1", "14257241###[Seite 237]###236###1", "14257242###[Seite 238]###237###1", "14257243###[Seite 239]###238###1", "14257244###[Seite 240]###239###1", "14257245###[Seite 241]###240###1", "14257246###[Seite 242]###241###1", "14257247###[Seite 243]###242###1", "14257248###[Seite 244]###243###1", "14257249###[Seite 245]###244###1", "14257250###[Seite 246]###245###1", "14257251###[Seite 247]###246###1", "14257252###[Seite 248]###247###1", "14257253###[Seite 249]###248###1", "14257254###[Seite 250]###249###1", "14257255###[Seite 251]###250###1", "14257256###[Seite 252]###251###1", "14257257###[Seite 253]###252###1", "14257258###[Seite 254]###253###1", "14257259###[Seite 255]###254###1", "14257260###[Seite 256]###255###1", "14257261###[Seite 257]###256###1", "14257262###[Seite 258]###257###1", "14257263###[Seite 259]###258###1", "14257264###[Seite 260]###259###1", "14257265###[Seite 261]###260###1", "14257266###[Seite 262]###261###1", "14257267###[Seite 263]###262###1", "14257268###[Seite 264]###263###1", "14257269###[Seite 265]###264###1", "14257270###[Seite 266]###265###1", "14257271###[Seite 267]###266###1", "14257272###[Seite 268]###267###1", "14257273###[Seite 269]###268###1", "14257274###[Seite 270]###269###1", "14257275###[Seite 271]###270###1", "14257276###[Seite 272]###271###1", "14257277###[Seite 273]###272###1", "14257278###[Seite 274]###273###1", "14257279###[Seite 275]###274###1", "14257280###[Seite 276]###275###1", "14257281###[Seite 277]###276###1", "14257282###[Seite 278]###277###1", "14257283###[Seite 279]###278###1", "14257284###[Seite 280]###279###1", "14257285###[Seite 281]###280###1", "14257286###[Seite 282]###281###1", "14257287###[Seite 283]###282###1", "14257288###[Seite 284]###283###1", "14257289###[Seite 285]###284###1", "14257290###[Seite 286]###285###1", "14257291###[Seite 287]###286###1", "14257292###[Seite 288]###287###1", "14257293###[Seite 289]###288###1", "14257294###[Seite 290]###289###1", "14257295###[Seite 291]###290###1", "14257296###[Seite 292]###291###1", "14257297###[Seite 293]###292###1", "14257298###[Seite 294]###293###1", "14257299###[Seite 295]###294###1")

    assert(pageDetails2 == expected2)
  }


  test("Get digital object page details - e-manuscripta") {
    val xmlElement = getXmlElemFromResourcesFolder("record/mongo-emanuscripta.xml")
    val pageDetails = getDigitalObjectPagesDetails(xmlElement)
    val expected = List("2447202###[Seite 5]###1###1", "2447203###[Seite 6]###2###1", "2447204###[Seite 7]###3###1", "2447205###[Seite 8]###4###1")

    assert(pageDetails == expected)
  }

  test("Extract page info") {
    val fulltextAvailability = List("2940906")
    val xmlElement =
      <mets:div
        ORDERLABEL="2"
        ORDER="1"
        CONTENTIDS="10.3931/e-rara-10016?urlappend=%3fpage=2940906"
        LABEL="Scan 2"
        TYPE="page"
        ID="phys2940906"
        xmlns:vl="http://visuallibrary.net/vl"
        xmlns:mods="http://www.loc.gov/mods/v3"
        xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
        xmlns:mets="http://www.loc.gov/METS/"
        xmlns:xlink="http://www.w3.org/1999/xlink">
          <mets:fptr FILEID="IMG_DEFAULT_2940906"/>
          <mets:fptr FILEID="IMG_THUMBS_2940906"/>
          <mets:fptr FILEID="IMG_MIN_2940906"/>
          <mets:fptr FILEID="IMG_MAX_2940906"/>
      </mets:div>
    val pageDetail = extractPageInformation(xmlElement, fulltextAvailability)

    assert(pageDetail == "2940906###Scan 2###1###1")

    val fulltextAvailability2 = List("412153454545")

    val pageDetail2 = extractPageInformation(xmlElement, fulltextAvailability2)
    assert(pageDetail2 == "2940906###Scan 2###1###0")

  }

  test("Get list of available fulltexts") {
    val xmlElement = getXmlElemFromResourcesFolder("record/mongo-erara-3.xml")
    val availableFulltexts = getListOfAvailableFulltexts(xmlElement)

    val expected = List("14257006", "14257007", "14257008", "14257009", "14257010", "14257011", "14257012", "14257013", "14257014", "14257015", "14257016", "14257017", "14257018", "14257019", "14257020", "14257021", "14257022", "14257023", "14257024", "14257025", "14257026", "14257027", "14257028", "14257029", "14257030", "14257031", "14257032", "14257033", "14257034", "14257035", "14257036", "14257037", "14257038", "14257039", "14257040", "14257041", "14257042", "14257043", "14257044", "14257045", "14257046", "14257047", "14257048", "14257049", "14257050", "14257051", "14257052", "14257053", "14257054", "14257055", "14257056", "14257057", "14257058", "14257059", "14257060", "14257061", "14257062", "14257063", "14257064", "14257065", "14257066", "14257067", "14257068", "14257069", "14257070", "14257071", "14257072", "14257073", "14257074", "14257075", "14257076", "14257077", "14257078", "14257079", "14257080", "14257081", "14257082", "14257083", "14257084", "14257085", "14257086", "14257087", "14257088", "14257089", "14257090", "14257091", "14257092", "14257093", "14257094", "14257095", "14257096", "14257097", "14257098", "14257099", "14257100", "14257101", "14257102", "14257103", "14257104", "14257105", "14257106", "14257107", "14257108", "14257109", "14257110", "14257111", "14257112", "14257113", "14257114", "14257115", "14257116", "14257117", "14257118", "14257119", "14257120", "14257121", "14257122", "14257123", "14257124", "14257125", "14257126", "14257127", "14257128", "14257129", "14257130", "14257131", "14257132", "14257133", "14257134", "14257135", "14257136", "14257137", "14257138", "14257139", "14257140", "14257141", "14257142", "14257143", "14257144", "14257145", "14257146", "14257147", "14257148", "14257149", "14257150", "14257151", "14257152", "14257153", "14257154", "14257155", "14257156", "14257157", "14257158", "14257159", "14257160", "14257161", "14257162", "14257163", "14257164", "14257165", "14257166", "14257167", "14257168", "14257169", "14257170", "14257171", "14257172", "14257173", "14257174", "14257175", "14257176", "14257177", "14257178", "14257179", "14257180", "14257181", "14257182", "14257183", "14257184", "14257185", "14257186", "14257187", "14257188", "14257189", "14257190", "14257191", "14257192", "14257193", "14257194", "14257195", "14257196", "14257197", "14257198", "14257199", "14257200", "14257201", "14257202", "14257203", "14257204", "14257205", "14257206", "14257207", "14257208", "14257209", "14257210", "14257211", "14257212", "14257213", "14257214", "14257215", "14257216", "14257217", "14257218", "14257219", "14257220", "14257221", "14257222", "14257223", "14257224", "14257225", "14257226", "14257227", "14257228", "14257229", "14257230", "14257231", "14257232", "14257233", "14257234", "14257235", "14257236", "14257237", "14257238", "14257239", "14257240", "14257241", "14257242", "14257243", "14257244", "14257245", "14257246", "14257247", "14257248", "14257249", "14257250", "14257251", "14257252", "14257253", "14257254", "14257255", "14257256", "14257257", "14257258", "14257259", "14257260", "14257261", "14257262", "14257263", "14257264", "14257265", "14257266", "14257267", "14257268", "14257269", "14257270", "14257271", "14257272", "14257273", "14257274", "14257275", "14257276", "14257277", "14257278", "14257279", "14257280", "14257281", "14257282", "14257283", "14257284", "14257285", "14257286", "14257287", "14257288", "14257289", "14257290", "14257291", "14257292", "14257293", "14257294", "14257295", "14257296", "14257297", "14257298", "14257299")

    assert(availableFulltexts == expected)
  }

  test("Get list of available fulltexts - no fulltext") {
    val xmlElement = getXmlElemFromResourcesFolder("record/mongo-erara.xml")
    val availableFulltexts = getListOfAvailableFulltexts(xmlElement)

    assert(availableFulltexts.isEmpty)
  }

  test("Get list of available fulltexts - no fulltext 2") {
    val xmlElement = getXmlElemFromResourcesFolder("record/mongo-erara-2.xml")
    val availableFulltexts = getListOfAvailableFulltexts(xmlElement)

    assert(availableFulltexts.isEmpty)
  }

  test("Get fulltext id") {
    val xmlElement = <mets:file MIMETYPE="text/xml" CREATED="2021-11-19T01:25:42.612300Z" ID="ALTO14257006">
      <mets:FLocat xlink:href="https://www.e-rara.ch/download/fulltext/alto3/14257006" LOCTYPE="URL"/>
    </mets:file>

    val id = getFulltextId(xmlElement)
    assert(id == "14257006")
  }

  test("Get fulltext id e-manuscripta") {
    val xmlElement = <mets:file MIMETYPE="text/markdown" CREATED="2020-02-24T09:55:44.821000Z" ID="TR1831288">
        <mets:FLocat xlink:href="https://www.e-manuscripta.ch/download/fulltext/raw/1831288" LOCTYPE="URL"/>
      </mets:file>

    val id = getFulltextId(xmlElement)
    assert(id == "1831288")
  }

  test("Get list of available fulltexts - e-manuscripta") {
    val xmlElement = getXmlElemFromResourcesFolder("record/mongo-emanuscripta.xml")
    val availableFulltexts = getListOfAvailableFulltexts(xmlElement)

    assert(availableFulltexts == List("2447202", "2447203", "2447204", "2447205"))
  }

  test("Get Added Fields") {
    val xmlElement = getXmlElemFromResourcesFolder("record/mongo-erara.xml")
    val addedFields = getAddedFields(xmlElement, "10.3931/e-rara-10016")
    assert(addedFields == "<field name=\"digital_platform_str_mv\">e-rara.ch</field><field name=\"id_digital_platform_str_mv\">2940903</field><field name=\"iiif_manifest_url_str_mv\">https://www.e-rara.ch/i3f/v20/2940903/manifest</field><field name=\"digital_reproduction_licence_str_mv\">pdm</field><field name=\"digital_reproduction_right_owner_str_mv\">Zentralbibliothek Zürich</field><field name=\"digital_reproduction_call_number_str_mv\">6.134,9</field><field name=\"digital_object_pages_details_str_mv\">2940906###Scan 2 &amp; irgendwas###1###0</field><field name=\"digital_object_pages_details_str_mv\">2940907###Scan 3###2###0</field><field name=\"digital_object_pages_details_str_mv\">2940908###Scan 4###3###0</field><field name=\"digital_object_pages_details_str_mv\">2940909###Scan 5###4###0</field><field name=\"digital_object_pages_details_str_mv\">2940910###Scan 6###5###0</field><field name=\"digital_object_pages_details_str_mv\">2940911###Scan 7###6###0</field><field name=\"digital_object_pages_details_str_mv\">2940912###Scan 8###7###0</field><field name=\"digital_object_pages_details_str_mv\">2940913###Scan 9###8###0</field>")
    println(addedFields)

    val xmlElement2 = getXmlElemFromResourcesFolder("record/mongo-erara-2.xml")
    val addedFields2 = getAddedFields(xmlElement2, "10.3931/e-rara-10016")
    assert(addedFields2 =="<field name=\"digital_platform_str_mv\">e-rara.ch</field><field name=\"id_digital_platform_str_mv\">1752282</field><field name=\"iiif_manifest_url_str_mv\">https://www.e-rara.ch/i3f/v20/1752282/manifest</field><field name=\"digital_reproduction_licence_str_mv\">pdm</field><field name=\"digital_reproduction_right_owner_str_mv\">Bibliothèque de Genève</field><field name=\"digital_reproduction_call_number_str_mv\">M&amp;P 3: 246</field><field name=\"digital_object_pages_details_str_mv\">12643760###[Seite 1]###1###0</field><field name=\"digital_object_pages_details_str_mv\">12643761###[Seite 2]###2###0</field><field name=\"digital_object_pages_details_str_mv\">12643762###[Seite 3]###3###0</field><field name=\"digital_object_pages_details_str_mv\">12643763###[Seite 4]###4###0</field><field name=\"digital_object_pages_details_str_mv\">12643764###[Seite 5]###5###0</field><field name=\"digital_object_pages_details_str_mv\">12643765###[Seite 6]###6###0</field><field name=\"digital_object_pages_details_str_mv\">12643766###[Seite 7]###7###0</field><field name=\"digital_object_pages_details_str_mv\">12643767###[Seite 8]###8###0</field><field name=\"digital_object_pages_details_str_mv\">12643768###[Seite 9]###9###0</field><field name=\"digital_object_pages_details_str_mv\">12643769###[Seite 10]###10###0</field><field name=\"digital_object_pages_details_str_mv\">12643770###[Seite 11]###11###0</field><field name=\"digital_object_pages_details_str_mv\">12643771###[Seite 12]###12###0</field><field name=\"digital_object_pages_details_str_mv\">12643772###[Seite 13]###13###0</field><field name=\"digital_object_pages_details_str_mv\">12643773###[Seite 14]###14###0</field><field name=\"digital_object_pages_details_str_mv\">12643774###[Seite 15]###15###0</field><field name=\"digital_object_pages_details_str_mv\">12643775###[Seite 16]###16###0</field><field name=\"digital_object_pages_details_str_mv\">12643776###[Seite 17]###17###0</field><field name=\"digital_object_pages_details_str_mv\">12643777###[Seite 18]###18###0</field><field name=\"digital_object_pages_details_str_mv\">12643778###[Seite 19]###19###0</field><field name=\"digital_object_pages_details_str_mv\">12643779###[Seite 20]###20###0</field><field name=\"digital_object_pages_details_str_mv\">12643780###[Seite 21]###21###0</field><field name=\"digital_object_pages_details_str_mv\">12643781###[Seite 22]###22###0</field><field name=\"digital_object_pages_details_str_mv\">12643782###[Seite 23]###23###0</field><field name=\"digital_object_pages_details_str_mv\">12643783###[Seite 24]###24###0</field><field name=\"digital_object_pages_details_str_mv\">12643784###[Seite 25]###25###0</field><field name=\"digital_object_pages_details_str_mv\">12643785###[Seite 26]###26###0</field><field name=\"digital_object_pages_details_str_mv\">12643786###[Seite 27]###27###0</field><field name=\"digital_object_pages_details_str_mv\">12643787###[Seite 28]###28###0</field><field name=\"digital_object_pages_details_str_mv\">12643788###[Seite 29]###29###0</field><field name=\"digital_object_pages_details_str_mv\">12643789###[Seite 30]###30###0</field><field name=\"digital_object_pages_details_str_mv\">12643790###[Seite 31]###31###0</field><field name=\"digital_object_pages_details_str_mv\">12643791###[Seite 32]###32###0</field><field name=\"digital_object_pages_details_str_mv\">12643792###[Seite 33]###33###0</field><field name=\"digital_object_pages_details_str_mv\">12643793###[Seite 34]###34###0</field><field name=\"digital_object_pages_details_str_mv\">12643794###[Seite 35]###35###0</field><field name=\"digital_object_pages_details_str_mv\">12643795###[Seite 36]###36###0</field><field name=\"digital_object_pages_details_str_mv\">12643796###[Seite 37]###37###0</field><field name=\"digital_object_pages_details_str_mv\">12643797###[Seite 38]###38###0</field><field name=\"digital_object_pages_details_str_mv\">12643798###[Seite 39]###39###0</field><field name=\"digital_object_pages_details_str_mv\">12643799###[Seite 40]###40###0</field><field name=\"digital_object_pages_details_str_mv\">12643800###[Seite 41]###41###0</field><field name=\"digital_object_pages_details_str_mv\">12643801###[Seite 42]###42###0</field><field name=\"digital_object_pages_details_str_mv\">12643802###[Seite 43]###43###0</field><field name=\"digital_object_pages_details_str_mv\">12643803###[Seite 44]###44###0</field><field name=\"digital_object_pages_details_str_mv\">12643804###[Seite 45]###45###0</field><field name=\"digital_object_pages_details_str_mv\">12643805###[Seite 46]###46###0</field><field name=\"digital_object_pages_details_str_mv\">12643806###[Seite 47]###47###0</field><field name=\"digital_object_pages_details_str_mv\">12643807###[Seite 48]###48###0</field><field name=\"digital_object_pages_details_str_mv\">12643808###[Seite 49]###49###0</field><field name=\"digital_object_pages_details_str_mv\">12643809###[Seite 50]###50###0</field><field name=\"digital_object_pages_details_str_mv\">12643810###[Seite 51]###51###0</field><field name=\"digital_object_pages_details_str_mv\">12643811###[Seite 52]###52###0</field><field name=\"digital_object_pages_details_str_mv\">12643812###[Seite 53]###53###0</field><field name=\"digital_object_pages_details_str_mv\">12643813###[Seite 54]###54###0</field><field name=\"digital_object_pages_details_str_mv\">12643814###[Seite 55]###55###0</field><field name=\"digital_object_pages_details_str_mv\">12643815###[Seite 56]###56###0</field><field name=\"digital_object_pages_details_str_mv\">12643816###[Seite 57]###57###0</field><field name=\"digital_object_pages_details_str_mv\">12643817###[Seite 58]###58###0</field><field name=\"digital_object_pages_details_str_mv\">12643818###[Seite 59]###59###0</field><field name=\"digital_object_pages_details_str_mv\">12643819###[Seite 60]###60###0</field><field name=\"digital_object_pages_details_str_mv\">12643820###[Seite 61]###61###0</field><field name=\"digital_object_pages_details_str_mv\">12643821###[Seite 62]###62###0</field><field name=\"digital_object_pages_details_str_mv\">12643822###[Seite 63]###63###0</field><field name=\"digital_object_pages_details_str_mv\">12643823###[Seite 64]###64###0</field><field name=\"digital_object_pages_details_str_mv\">12643824###[Seite 65]###65###0</field><field name=\"digital_object_pages_details_str_mv\">12643825###[Seite 66]###66###0</field><field name=\"digital_object_pages_details_str_mv\">12643826###[Seite 67]###67###0</field><field name=\"digital_object_pages_details_str_mv\">12643827###[Seite 68]###68###0</field><field name=\"digital_object_pages_details_str_mv\">12643828###[Seite 69]###69###0</field><field name=\"digital_object_pages_details_str_mv\">12643829###[Seite 70]###70###0</field><field name=\"digital_object_pages_details_str_mv\">12643830###[Seite 71]###71###0</field><field name=\"digital_object_pages_details_str_mv\">12643831###[Seite 72]###72###0</field><field name=\"digital_object_pages_details_str_mv\">12643832###[Seite 73]###73###0</field><field name=\"digital_object_pages_details_str_mv\">12643833###[Seite 74]###74###0</field><field name=\"digital_object_pages_details_str_mv\">12643834###[Seite 75]###75###0</field><field name=\"digital_object_pages_details_str_mv\">12643835###[Seite 76]###76###0</field><field name=\"digital_object_pages_details_str_mv\">12643836###[Seite 77]###77###0</field><field name=\"digital_object_pages_details_str_mv\">12643837###[Seite 78]###78###0</field><field name=\"digital_object_pages_details_str_mv\">12643838###[Seite 79]###79###0</field><field name=\"digital_object_pages_details_str_mv\">12643839###[Seite 80]###80###0</field><field name=\"digital_object_pages_details_str_mv\">12643840###[Seite 81]###81###0</field><field name=\"digital_object_pages_details_str_mv\">12643841###[Seite 82]###82###0</field><field name=\"digital_object_pages_details_str_mv\">12643842###[Seite 83]###83###0</field><field name=\"digital_object_pages_details_str_mv\">12643843###[Seite 84]###84###0</field><field name=\"digital_object_pages_details_str_mv\">12643844###[Seite 85]###85###0</field><field name=\"digital_object_pages_details_str_mv\">12643845###[Seite 86]###86###0</field><field name=\"digital_object_pages_details_str_mv\">12643846###[Seite 87]###87###0</field><field name=\"digital_object_pages_details_str_mv\">12643847###[Seite 88]###88###0</field><field name=\"digital_object_pages_details_str_mv\">12643848###[Seite 89]###89###0</field><field name=\"digital_object_pages_details_str_mv\">12643849###[Seite 90]###90###0</field><field name=\"digital_object_pages_details_str_mv\">12643850###[Seite 91]###91###0</field><field name=\"digital_object_pages_details_str_mv\">12643851###[Seite 92]###92###0</field><field name=\"digital_object_pages_details_str_mv\">12643852###[Seite 93]###93###0</field><field name=\"digital_object_pages_details_str_mv\">12643853###[Seite 94]###94###0</field><field name=\"digital_object_pages_details_str_mv\">12643854###[Seite 95]###95###0</field><field name=\"digital_object_pages_details_str_mv\">12643855###[Seite 96]###96###0</field><field name=\"digital_object_pages_details_str_mv\">12643856###[Seite 97]###97###0</field><field name=\"digital_object_pages_details_str_mv\">12643857###[Seite 98]###98###0</field><field name=\"digital_object_pages_details_str_mv\">12643858###[Seite 99]###99###0</field><field name=\"digital_object_pages_details_str_mv\">12643859###[Seite 100]###100###0</field><field name=\"digital_object_pages_details_str_mv\">12643860###[Seite 101]###101###0</field><field name=\"digital_object_pages_details_str_mv\">12643861###[Seite 102]###102###0</field><field name=\"digital_object_pages_details_str_mv\">12643862###[Seite 103]###103###0</field><field name=\"digital_object_pages_details_str_mv\">12643863###[Seite 104]###104###0</field><field name=\"digital_object_pages_details_str_mv\">12643864###[Seite 105]###105###0</field><field name=\"digital_object_pages_details_str_mv\">12643865###[Seite 106]###106###0</field><field name=\"digital_object_pages_details_str_mv\">12643866###[Seite 107]###107###0</field><field name=\"digital_object_pages_details_str_mv\">12643867###[Seite 108]###108###0</field><field name=\"digital_object_pages_details_str_mv\">12643868###[Seite 109]###109###0</field><field name=\"digital_object_pages_details_str_mv\">12643869###[Seite 110]###110###0</field><field name=\"digital_object_pages_details_str_mv\">12643870###[Seite 111]###111###0</field><field name=\"digital_object_pages_details_str_mv\">12643871###[Seite 112]###112###0</field><field name=\"digital_object_pages_details_str_mv\">12643872###[Seite 113]###113###0</field><field name=\"digital_object_pages_details_str_mv\">12643873###[Seite 114]###114###0</field><field name=\"digital_object_pages_details_str_mv\">12643874###[Seite 115]###115###0</field><field name=\"digital_object_pages_details_str_mv\">12643875###[Seite 116]###116###0</field><field name=\"digital_object_pages_details_str_mv\">12643876###[Seite 117]###117###0</field><field name=\"digital_object_pages_details_str_mv\">12643877###[Seite 118]###118###0</field><field name=\"digital_object_pages_details_str_mv\">12643878###[Seite 119]###119###0</field><field name=\"digital_object_pages_details_str_mv\">12643879###[Seite 120]###120###0</field><field name=\"digital_object_pages_details_str_mv\">12643880###[Seite 121]###121###0</field><field name=\"digital_object_pages_details_str_mv\">12643881###[Seite 122]###122###0</field><field name=\"digital_object_pages_details_str_mv\">12643882###[Seite 123]###123###0</field><field name=\"digital_object_pages_details_str_mv\">12643883###[Seite 124]###124###0</field><field name=\"digital_object_pages_details_str_mv\">12643884###[Seite 125]###125###0</field><field name=\"digital_object_pages_details_str_mv\">12643885###[Seite 126]###126###0</field><field name=\"digital_object_pages_details_str_mv\">12643886###[Seite 127]###127###0</field><field name=\"digital_object_pages_details_str_mv\">12643887###[Seite 128]###128###0</field><field name=\"digital_object_pages_details_str_mv\">12643888###[Seite 129]###129###0</field><field name=\"digital_object_pages_details_str_mv\">12643889###[Seite 130]###130###0</field><field name=\"digital_object_pages_details_str_mv\">12643890###[Seite 131]###131###0</field><field name=\"digital_object_pages_details_str_mv\">12643891###[Seite 132]###132###0</field><field name=\"digital_object_pages_details_str_mv\">12643892###[Seite 133]###133###0</field><field name=\"digital_object_pages_details_str_mv\">12643893###[Seite 134]###134###0</field><field name=\"digital_object_pages_details_str_mv\">12643894###[Seite 135]###135###0</field><field name=\"digital_object_pages_details_str_mv\">12643895###[Seite 136]###136###0</field><field name=\"digital_object_pages_details_str_mv\">12643896###[Seite 137]###137###0</field><field name=\"digital_object_pages_details_str_mv\">12643897###[Seite 138]###138###0</field><field name=\"digital_object_pages_details_str_mv\">12643898###[Seite 139]###139###0</field><field name=\"digital_object_pages_details_str_mv\">12643899###[Seite 140]###140###0</field><field name=\"digital_object_pages_details_str_mv\">12643900###[Seite 141]###141###0</field><field name=\"digital_object_pages_details_str_mv\">12643901###[Seite 142]###142###0</field><field name=\"digital_object_pages_details_str_mv\">12643902###[Seite 143]###143###0</field><field name=\"digital_object_pages_details_str_mv\">12643903###[Seite 144]###144###0</field><field name=\"digital_object_pages_details_str_mv\">12643904###[Seite 145]###145###0</field><field name=\"digital_object_pages_details_str_mv\">12643905###[Seite 146]###146###0</field><field name=\"digital_object_pages_details_str_mv\">12643906###[Seite 147]###147###0</field><field name=\"digital_object_pages_details_str_mv\">12643907###[Seite 148]###148###0</field><field name=\"digital_object_pages_details_str_mv\">12643908###[Seite 149]###149###0</field><field name=\"digital_object_pages_details_str_mv\">12643909###[Seite 150]###150###0</field><field name=\"digital_object_pages_details_str_mv\">12643910###[Seite 151]###151###0</field><field name=\"digital_object_pages_details_str_mv\">12643911###[Seite 152]###152###0</field><field name=\"digital_object_pages_details_str_mv\">12643912###[Seite 153]###153###0</field><field name=\"digital_object_pages_details_str_mv\">12643913###[Seite 154]###154###0</field><field name=\"digital_object_pages_details_str_mv\">12643914###[Seite 155]###155###0</field><field name=\"digital_object_pages_details_str_mv\">12643915###[Seite 156]###156###0</field><field name=\"digital_object_pages_details_str_mv\">12643916###[Seite 157]###157###0</field><field name=\"digital_object_pages_details_str_mv\">12643917###[Seite 158]###158###0</field><field name=\"digital_object_pages_details_str_mv\">12643918###[Seite 159]###159###0</field><field name=\"digital_object_pages_details_str_mv\">12643919###[Seite 160]###160###0</field><field name=\"digital_object_pages_details_str_mv\">12643920###[Seite 161]###161###0</field><field name=\"digital_object_pages_details_str_mv\">12643921###[Seite 162]###162###0</field><field name=\"digital_object_pages_details_str_mv\">12643922###[Seite 163]###163###0</field><field name=\"digital_object_pages_details_str_mv\">12643923###[Seite 164]###164###0</field><field name=\"digital_object_pages_details_str_mv\">12643924###[Seite 165]###165###0</field><field name=\"digital_object_pages_details_str_mv\">12643925###[Seite 166]###166###0</field><field name=\"digital_object_pages_details_str_mv\">12643926###[Seite 167]###167###0</field><field name=\"digital_object_pages_details_str_mv\">12643927###[Seite 168]###168###0</field><field name=\"digital_object_pages_details_str_mv\">12643928###[Seite 169]###169###0</field><field name=\"digital_object_pages_details_str_mv\">12643929###[Seite 170]###170###0</field><field name=\"digital_object_pages_details_str_mv\">12643930###[Seite 171]###171###0</field><field name=\"digital_object_pages_details_str_mv\">12643931###[Seite 172]###172###0</field><field name=\"digital_object_pages_details_str_mv\">12643932###[Seite 173]###173###0</field><field name=\"digital_object_pages_details_str_mv\">12643933###[Seite 174]###174###0</field><field name=\"digital_object_pages_details_str_mv\">12643934###[Seite 175]###175###0</field><field name=\"digital_object_pages_details_str_mv\">12643935###[Seite 176]###176###0</field><field name=\"digital_object_pages_details_str_mv\">12643936###[Seite 177]###177###0</field><field name=\"digital_object_pages_details_str_mv\">12643937###[Seite 178]###178###0</field><field name=\"digital_object_pages_details_str_mv\">12643938###[Seite 179]###179###0</field><field name=\"digital_object_pages_details_str_mv\">12643939###[Seite 180]###180###0</field><field name=\"digital_object_pages_details_str_mv\">12643940###[Seite 181]###181###0</field><field name=\"digital_object_pages_details_str_mv\">12643941###[Seite 182]###182###0</field><field name=\"digital_object_pages_details_str_mv\">12643942###[Seite 183]###183###0</field><field name=\"digital_object_pages_details_str_mv\">12643943###[Seite 184]###184###0</field><field name=\"digital_object_pages_details_str_mv\">12643944###[Seite 185]###185###0</field><field name=\"digital_object_pages_details_str_mv\">12643945###[Seite 186]###186###0</field><field name=\"digital_object_pages_details_str_mv\">12643946###[Seite 187]###187###0</field><field name=\"digital_object_pages_details_str_mv\">12643947###[Seite 188]###188###0</field><field name=\"digital_object_pages_details_str_mv\">12643948###[Seite 189]###189###0</field><field name=\"digital_object_pages_details_str_mv\">12643949###[Seite 190]###190###0</field><field name=\"digital_object_pages_details_str_mv\">12643950###[Seite 191]###191###0</field><field name=\"digital_object_pages_details_str_mv\">12643951###[Seite 192]###192###0</field><field name=\"digital_object_pages_details_str_mv\">12643952###[Seite 193]###193###0</field><field name=\"digital_object_pages_details_str_mv\">12643953###[Seite 194]###194###0</field><field name=\"digital_object_pages_details_str_mv\">12643954###[Seite 195]###195###0</field><field name=\"digital_object_pages_details_str_mv\">12643955###[Seite 196]###196###0</field><field name=\"digital_object_pages_details_str_mv\">12643956###[Seite 197]###197###0</field><field name=\"digital_object_pages_details_str_mv\">12643957###[Seite 198]###198###0</field>")
  }

  test("Get Added Fields - e-manuscripta") {
    val xmlElement3 = getXmlElemFromResourcesFolder("record/mongo-emanuscripta.xml")
    val addedFields3 = getAddedFields(xmlElement3, "10.7891/e-manuscripta-86675")

    assert(addedFields3 == "<field name=\"digital_platform_str_mv\">e-manuscripta.ch</field><field name=\"id_digital_platform_str_mv\">2458673</field><field name=\"iiif_manifest_url_str_mv\">https://www.e-manuscripta.ch/i3f/v20/2458673/manifest</field><field name=\"digital_reproduction_licence_str_mv\">pdm</field><field name=\"digital_reproduction_right_owner_str_mv\">Zentralbibliothek Zürich</field><field name=\"digital_reproduction_call_number_str_mv\">FA Lav Ms 502.242FA Lav Ms 502.241-242</field><field name=\"digital_object_pages_details_str_mv\">2447202###[Seite 5]###1###1</field><field name=\"digital_object_pages_details_str_mv\">2447203###[Seite 6]###2###1</field><field name=\"digital_object_pages_details_str_mv\">2447204###[Seite 7]###3###1</field><field name=\"digital_object_pages_details_str_mv\">2447205###[Seite 8]###4###1</field>")
  }

  //needs a mongo connection
  ignore("Full process") {
    val solrRecord = getTextFromResourcesFolder("record/solrdoc-erara.xml")
    val enrichedRecord = enrichWithERaraEManuscriptaInfo(solrRecord)
    val expected = getTextFromResourcesFolder("result/solrdoc-erara-enriched.xml")
    XML.loadString(enrichedRecord) should equal (XML.loadString(expected)) (after being streamlined[Elem])
  }
}
