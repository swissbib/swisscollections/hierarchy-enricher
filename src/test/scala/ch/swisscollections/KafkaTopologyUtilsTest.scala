/*
 * media-metadata-extractor
 * Copyright (C) 2020  Memoriav
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package ch.swisscollections

import ch.swisscollections.KafkaTopologyUtils._
import org.scalatest.Tag
import org.scalatest.funsuite.AnyFunSuite

import scala.io.Source
import scala.xml.XML

object NeedsSolrConnection extends Tag("ch.swisscollections.tags.NeedsSolrConnection")


class KafkaTopologyUtilsTest extends AnyFunSuite {

  private def getTextFromResourcesFolder(filepath: String): String = {
    val file = Source.fromFile(s"src/test/resources/$filepath")
    val text = file.mkString
    file.close()
    text
  }

  test("Test Extract Parent Id") {
    val xmlString = getTextFromResourcesFolder("record/solrdoc.xml")
    val parentId = getParentId(xmlString)
    assert(parentId == "(IDSBB)001261164DSV01")
  }

  test("Test Extract Parent Id where no parent are available") {
    val xmlString = getTextFromResourcesFolder("record/solrdoc-no-parent.xml")
    val parentId = getParentId(xmlString)
    assert(parentId == "")
  }

  //needs a solr cluster available to test this or we should mock it
  test("Test Get Top Ancestor Id", NeedsSolrConnection) {
    val xmlString = getTextFromResourcesFolder("record/solrdoc-cullmann.xml")
    val parentId = getParentId(xmlString)
    val topAncestorId = getTopAncestorId(parentId)
    assert(topAncestorId == "991170430612805501")
  }

  //needs a solr cluster available to test this or we should mock it
  test("Test Full Process", NeedsSolrConnection) {
    val xmlString = getTextFromResourcesFolder("record/solrdoc-cullmann.xml")
    val record = hierarchyEnriched(xmlString)
    assert(record.contains("<field name=\"hierarchy_top_id\">" + "991170430612805501" + "</field>"))
    assert(record.contains("<field name=\"hierarchy_parent_id\">" + "991170431221005501" + "</field>"))
    assert(!record.contains("<field name=\"hierarchy_parent_id\">(HAN)000187609DSV05</field>"))
    print(record)
  }

  //needs a solr cluster available to test this or we should mock it
  test("Test Full Process with alma id at the start", NeedsSolrConnection) {
    val xmlString = getTextFromResourcesFolder("record/solrdoc-cullmann-with-AlmaId.xml")
    val record = hierarchyEnriched(xmlString)
    assert(record.contains("<field name=\"hierarchy_top_id\">" + "991170430612805501" + "</field>"))
    assert(!record.contains("<field name=\"hierarchy_parent_id\">(HAN)000187609DSV05</field>"))
    print(record)
  }


  //needs a solr cluster available to test this or we should mock it
  test("Test Full Process with special title", NeedsSolrConnection) {
    val xmlString = getTextFromResourcesFolder("record/solrdoc-with-ampersand.xml")
    val record = hierarchyEnriched(xmlString)
    assert(record.contains("<field name=\"hierarchy_top_title\">" + "Cäcilia : geistliche und weltliche Arien&amp;Lieder älterer Meister für eine Singstimme mit Pianoforte" + "</field>"))
    print(record)
  }

  //needs a solr cluster available to test this or we should mock it
  test("Test Infinite loop when a record is the parent of himself", NeedsSolrConnection) {
    val xmlString = getTextFromResourcesFolder("record/solrdoc-autoparent.xml")
    val record = hierarchyEnriched(xmlString)
    //no enrichment should happen
    assert(record==xmlString)
    print(record)
  }

  //needs a solr cluster available to test this or we should mock it
  test("has Children", NeedsSolrConnection) {
    assert(hasChildren("991170430612805501"))
    assert(!hasChildren("991170351029605501"))
  }

  //needs a solr cluster available to test this or we should mock it
  test("mark archives without children", NeedsSolrConnection) {
    val amberg = getTextFromResourcesFolder("record/solrdoc-amberg.xml")
    val expectedResult = getTextFromResourcesFolder("result/solrdoc-amberg-result.xml")

    assert(markArchivesWithoutChildren(amberg) == expectedResult)

    val cullmann = getTextFromResourcesFolder("record/solrdoc-cullmann.xml")

    //As Cullmann has children, it should not be changed
    assert(markArchivesWithoutChildren(cullmann) == cullmann)
  }

  test("get lowest facet value") {
    val xmlString = getTextFromResourcesFolder("record/solrdoc-amberg.xml")
    val result = getLowestFacetValue(XML.loadString(xmlString), "tectonics_str_mv")
    assert (result.get == "2/LUZHB/Archive von Personen und Organisationen/Amberg, Anton (1802-1883)/")
  }

  test("get 2nd level facet value") {
    val xmlString = getTextFromResourcesFolder("record/solrdoc-amberg.xml")
    val result = get2ndLevelFacetValues(XML.loadString(xmlString), "topic_tectonic_str_mv")
    assert (result == List("1/Bildende Kunst/Amberg, Anton (1802-1883)/", "1/Musik/Amberg, Anton (1802-1883)/"))
  }

  test("cmi", NeedsSolrConnection) {
    val xmlString = getTextFromResourcesFolder("record/solrdoc-cmi.xml")
    val record = hierarchyEnriched(xmlString)

    assert(record.contains("<field name=\"id\">ZBC0854a9d7cd8d487fa041f31ec398b705</field>"))
    assert(record.contains("<field name=\"is_hierarchy_id\">ZBC0854a9d7cd8d487fa041f31ec398b705</field>"))
    assert(record.contains("<field name=\"is_hierarchy_title\">Gästebuch</field>"))
    assert(record.contains("<field name=\"hierarchy_top_id\">ZBC456a434678844681932ea8b55bb88e14</field>"))
    assert(record.contains("<field name=\"hierarchy_top_title\">Gasser, Manuel (1909 - 1979) : Nachlass</field>"))
    assert(record.contains("<field name=\"hierarchytype\">archival</field>"))
    assert(record.contains("<field name=\"hierarchy_parent_id\">ZBCa655f12e28134e2983c58bb0a09b80d8</field>"))
    assert(record.contains("<field name=\"hierarchy_parent_title\">Erinnerungsstücke</field>"))
    assert(record.contains("<field name=\"title_in_hierarchy\">Hs NL 1: Bd 1 : Gästebuch / Nachlass Manuel Gasser (1952-1957)</field>"))
    assert(record.contains("<field name=\"hierarchy_sequence\">Hs NL 00001: Bd 00001</field>"))
  }
}
