package ch.swisscollections

import org.scalatest.funsuite.AnyFunSuite

class MongoWrapperTest extends AnyFunSuite {
  ignore("Test Find By Doi") {
    val mongoClient = EnrichFromMongo.getMongoClient
    val xmlString = mongoClient.findByDoi("10.3931/e-rara-10016")
    assert (xmlString.get.contains("<mods:identifier type=\"doi\">10.3931/e-rara-10016</mods:identifier>"))

    val xmlString2 = mongoClient.findByDoi("10.7891/e-manuscripta-148731")
    assert (xmlString2.get.contains("<mods:identifier type=\"doi\">10.7891/e-manuscripta-148731</mods:identifier>"))

    val xmlString3 = mongoClient.findByDoi("whatever")
    assert (xmlString3 == None)
  }
}
