/*
 * enricher
 * Copyright (C) 2020  UB Basel
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package ch.swisscollections

import ch.swisscollections.App.logger
import ch.swisscollections.SolrJClientWrapper
import org.apache.logging.log4j.scala.Logging
import org.apache.solr.client.solrj.SolrClient
import org.apache.solr.client.solrj.impl.{CloudSolrClient, HttpSolrClient}
import ch.memobase.settings.SettingsLoader
import org.apache.solr.common.SolrDocument

import scala.::
import scala.util.{Failure, Success, Try}
import scala.xml.{Elem, Node, NodeSeq, SAXParseException, Utility, XML}
import scala.jdk.CollectionConverters._



object KafkaTopologyUtils extends Logging {

  private lazy val client = getSolrClient

  /**
   * Main method. Build the hierarchies and mark the archives without children
   *
   * @param record
   * @return
   */
  def hierarchyEnriched(record: String): String = {
    val recordWithHierarchy = buildHierarchies(record)
    markArchivesWithoutChildren(recordWithHierarchy)
  }

  /**
   * Some archives don't have children. Only the "Fonds=Bestand" is in the catalog,
   * not the child records. To ensure a good tectonics display, we need to mark
   * such records.
   *
   * Decision is to change the facet value of the fields tectonics_str_mv and
   * topic_tectonic_str_mv to add an underscore at the end
   *
   * Archives with children are not changed at all
   *
   * @param record
   * @return the record modified for archives without children
   */
  def markArchivesWithoutChildren(record: String): String = {
    val id = getId(record)
    if (hasChildren(id)) {
      record
    } else {
      //We add an underscore to the longest facet value
      //Probably we should rather parse xml and do a transformation
      val recordAsXML = XML.loadString(record)

      val tectonics = getLowestFacetValue(recordAsXML, "tectonics_str_mv")
      val topicTectonics = get2ndLevelFacetValues(recordAsXML, "topic_tectonic_str_mv")

      var stringsToReplace = topicTectonics
      if (tectonics.nonEmpty) {
        stringsToReplace = stringsToReplace :+ tectonics.get
      }

      //Adds an underscore at the end of all relevant facet values
      stringsToReplace.foldLeft(record)((string, replacement) => string.replace(replacement, replacement.substring(0, replacement.length-1) +"_/"))

    }
  }

  /**
   * Returns the lowest facet value of a hierarchical facet
   * For example for
   * <field name="tectonics_str_mv">2/LUZHB/Archive von Personen und Organisationen/Amberg, Anton (1802-1883)/</field>
   * <field name="tectonics_str_mv">1/LUZHB/Archive von Personen und Organisationen/</field>
   * <field name="tectonics_str_mv">0/LUZHB/</field>
   *
   * That will be the first one (starting with 2)
   *
   * @param recordAsXml the record
   * @param facet the facet we want to retrieve
   * @return
   */
  def getLowestFacetValue (recordAsXml: Node, facet: String): Option[String] = {
    val facetValues = for {
      field <- recordAsXml \\ "field"
      if field \@ "name" == facet
    } yield field.text
    //we need the facet value with the highest integer at the beginning
    if (facetValues.length>0) {
      Option(facetValues.sortWith(_ > _).head)
    } else {
      None
    }
  }

  /**
   * Returns an array of the values of the 2nd level (starting with 1) of a facet
   * For example for
   * <field name="topic_tectonic_str_mv">1/Bildende Kunst/Amberg, Anton (1802-1883)/</field>
   * <field name="topic_tectonic_str_mv">0/Bildende Kunst/</field>
   *
   * <field name="topic_tectonic_str_mv">1/Musik/Amberg, Anton (1802-1883)/</field>
   * <field name="topic_tectonic_str_mv">0/Musik/</field>
   *
   * It will return
   * [
   *  "1/Bildende Kunst/Amberg, Anton (1802-1883)/",
   *  "1/Musik/Amberg, Anton (1802-1883)/"
   * ]
   *
   * @param recordAsXml the record
   * @param facet the facet we want to retrieve
   * @return
   */
  def get2ndLevelFacetValues (recordAsXml: Node, facet: String): Seq[String] = {
    val facetValues = for {
      field <- recordAsXml \\ "field"
      if field \@ "name" == facet
    } yield field.text
    //we need the facet value with the highest integer at the beginning
    facetValues.filter(x=>x.startsWith("1"))
  }

  /**
   * Check if there are records in Solr where the hierarchy_parent_id has the value of the current
   * record
   *
   * @param recordId
   * @return
   */
  def hasChildren(recordId: String): Boolean = {
    client.hasChildren(recordId)
  }


  /**
   * Add, when relevant, the following fields to the record
   * <field name="hierarchy_top_id">
   * <field name="hierarchy_top_title">
   * <field name="hierarchytype">
   * <field name="hierarchy_parent_id">
   *
   * To do this we need to climb up the hierarchy up to the top ancestor
   * We do this using an already populated solr index
   *
   * The current hierarchy_parent_id is replaced by a solr id
   *
   * See https://vufind.org/wiki/indexing:hierarchies_and_collections
   * for more info
   *
   * @param record
   * @return
   */
  def buildHierarchies(record: String): String = {
    Try[String]   {
      val id = getId(record)
      if (id == "") {
        return record
      }
      val parentId = getParentId(record)
      if (parentId == "") {
        //logger.info(id+" has no parent")
        return record
      } else {
        val topAncestorId = getTopAncestorId(parentId)
        if (topAncestorId == "") {
          //some records not in solr or an infinite loop
          //todo write that to the catch-up topic
          //logger.info(parentId + " not in solr")
          return record
        } else {
          //enrich the record with informations from the Top Ancestor id
          //todo avoid that additional solr call
            recordWithUpdatedHierarchyInformation(record, parentId, topAncestorId)
          }
        }
      }

    } match {
      case Success(res) => res
      case Failure(exception) =>
        logger.error(s"record $record throws exception ${exception.getMessage}")
        //handover the original record
        record
    }


  private def recordWithUpdatedHierarchyInformation(record: String, parentId: String, topAncestorId: String): String = {
    val topAncestorRecords = client.searchBySolrId(topAncestorId)
    if (topAncestorRecords.isEmpty) {
      throw NotFoundInSolrException("ancestor not found in solr " + topAncestorId)
    }

    //we only consider the first ancestor for now
    //https://ub-basel.atlassian.net/browse/SC-298
    val topAncestorRecord = topAncestorRecords.head

    val topAncestorTitle = Utility.escape(topAncestorRecord.getFirstValue("title").toString)

    //enrich with ancestor info
    var textToAdd = "<field name=\"hierarchy_top_id\">" + topAncestorId + "</field>" +
      "<field name=\"hierarchy_top_title\">" + topAncestorTitle + "</field>" +
      "<field name=\"hierarchytype\">" + "archival" + "</field>"

    //now search the alma id of the parent to insert it in the current record
    val parents = getDocumentBasedOnAnyIdentifier(parentId)

    if (parents.isEmpty) {
      throw NotFoundInSolrException("parent not found in solr " + parentId)
    }

    val parentSolrId = parents.head.getFirstValue("id").toString
    //replace old id with alma id (solr id)
    textToAdd += "<field name=\"hierarchy_parent_id\">" + parentSolrId + "</field>"

    //escape parentheses
    val parentIdRegexp = parentId.replace("(", "\\(").replace(")", "\\)")

    val result = record.replaceFirst(
      "<field name=\"hierarchy_parent_id\">" + parentIdRegexp + "</field>",
      textToAdd
    )
    result
  }

  /**
   * Get the parent id from the solr document
   * Return empty string if no parent id found
   *
   * @param record
   * @return
   */
  def getParentId(record: String): String = {
    try {
      val xml = XML.loadString(record)

      //uses the first hierarchy_parent_id, if there are multiple hierarchy_parent_id
      //ignore the others
      val parentId = (xml \\ "field")
        .find(f => (f \@ "name") == "hierarchy_parent_id")

      parentId match {
        case Some(i) => parentId.get.text
        case None => ""
      }
    } catch {
        case e:SAXParseException =>
          throw XmlException(
            "Problem parsing xml from record " + record
          )
    }
  }

  /**
   * Get the id of the record
   *
   * @param record
   * @return
   */
  def getId(record: String): String = {
    try {
      val xml = XML.loadString(record)

      val parentId = (xml \\ "field")
        .find(f => (f \@ "name") == "id")

      parentId match {
        case Some(i) => parentId.get.text
        case None => ""
      }
    } catch {
      case e:SAXParseException =>
        throw XmlException(
          "Problem parsing xml from record " + record
        )
    }
  }

  /**
   * Return top ancestor id if available
   * If some parents are not in solr returns ""
   *
   * @param parentId the id of the parent record
   * @param level the number of level we climb before we exit the loop
   * @return string, the solr id of the top ancestor
   */
  def getTopAncestorId(parentId:String, level:Int=1): String = {
    if (level > 20) {
      //we should not get in an infinite loop when A is the father of B which might be by mistake the father of A
      logger.info("Infinite loop for this record" + parentId)
      return ""
    }

    getDocumentBasedOnAnyIdentifier(parentId) match {
      case Some(result) if result.getFirstValue("hierarchy_parent_id") != null => getTopAncestorId(result.getFirstValue("hierarchy_parent_id").toString, level+1) //the parent has a parent
      case Some(result) => result.getFirstValue("id").toString //the parent has no parent, therefore the parent is the top ancestor
      case None => "" //the parent is not in solr
    }
  }

  /**
   * Get a record based on a solr identifier or another identifier
   * (which should be in ctrlnum solr field)
   *
   * @param possibleIdentifier
   * @return the document
   */
  def getDocumentBasedOnAnyIdentifier(possibleIdentifier: String): Option[SolrDocument] = {
    client.searchByOtherIdentifier(possibleIdentifier) match {
      case results if results.nonEmpty => results.headOption
      case _ => client.searchBySolrId(possibleIdentifier).headOption
    }
  }

  def getSolrClient: SolrJClientWrapper = {
    val settings = new SettingsLoader(List(
      "mode",
      "zkhosts",
      "collection",
      "connectionTimeout",
      "socketTimeOut",
      "nodeURL",
      "zkChroot",
      "timeDurationForNextSolrCommit",
      "solrCommitBufferLength"
    ).asJava,
      "app.yml",
      false,
      true,
      false,
      false)

    val solrWrapper = SolrJClientWrapper(
      settings.getAppSettings.getProperty("mode"),
      settings.getAppSettings.getProperty("zkhosts"),
      settings.getAppSettings.getProperty("collection"),
      settings.getAppSettings.getProperty("connectionTimeout"),
      settings.getAppSettings.getProperty("socketTimeOut"),
      if (settings.getAppSettings.getProperty("zkChroot") == "NONE") Option.empty else
        Option(settings.getAppSettings.getProperty("zkChroot")) ,
      settings.getAppSettings.getProperty("nodeURL"),
      settings.getAppSettings.getProperty("solrCommitBufferLength"),
      settings.getAppSettings.getProperty("timeDurationForNextSolrCommit")
    )
    solrWrapper
  }

}
