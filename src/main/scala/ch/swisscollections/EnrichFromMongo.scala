/*
 * enricher
 * Copyright (C) 2020  UB Basel
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package ch.swisscollections

import ch.memobase.settings.SettingsLoader
import org.apache.logging.log4j.scala.Logging

import scala.jdk.CollectionConverters._
import scala.util.matching.Regex
import scala.xml.{Elem, Node, NodeSeq, SAXParseException, XML}


object EnrichFromMongo extends Logging {

  private lazy val mongoClient = getMongoClient

  /**
   * extract doi from kafka message
   * check if it is erara / emanuscripta
   * if no don't do anything
   * if yes get record from mongo based on doi
   * convert to xmlElement
   * enrich the record with erara / emanuscripta info
   * Details https://ub-basel.atlassian.net/wiki/spaces/RH/pages/2254012440/Implementation
   *
   * @param solrRecord
   * @return
   */
  def enrichWithERaraEManuscriptaInfo(solrRecord: String): String = {
    val solrRecordXML = try {
      XML.loadString(solrRecord)
    } catch {
      case e:SAXParseException =>
        throw XmlException(
          "Problem parsing xml from record " + solrRecord
        )
    }

    val doi = getEplatformDoi(solrRecordXML)

    doi match {
      case None => solrRecord
      case Some(value) =>
        val metsRecordString = mongoClient.findByDoi(value)

        metsRecordString match {
          case None => solrRecord
          case Some(value) =>
            val metsRecordXML = try {
              XML.loadString(value)
            } catch {
              case e:SAXParseException =>
                throw XmlException(
                  "Problem parsing xml from record " + metsRecordString
                )
            }

            val addedFields = doi match {
              case Some(value) => getAddedFields(metsRecordXML, value)
              case None => ""
            }
            solrRecord.replace("</doc>", addedFields + "</doc>")
        }
    }
  }

  /**
   * Adds
   * "digital_platform": ["e-rara.ch"] //not an array, if multiple platforms available, only one is listed !$
   * "id_digital_platform": ["2940903"], //not an array !
   * "iiif_manifest_url" : ["https://www.e-rara.ch/i3f/v20/2940903/manifest"],   //calculated, based on digital_object_id, platform and iiif prefix
   * "digital_reproduction_licence" : ["pdm"], //rightsMD
   * "digital_reproduction_right_owner" : ["Universitätsbibliothek Basel"], //rightsMD
   * "digital_reproduction_call_number" : [""], //mets shelflocator (2 places), not available for this example
   * "digital_object_pages_details": [
   * "2940906###Scan 2###1###0",    //ID, LABEL, ORDER, Fulltext availability aus METS separated with ###
   * "2940907###Scan 3###2###0",
   * ...
   * "2940912###Scan 8###7###1",
   * ...
   * ]
   * @param metsRecordXML
   * @param doi
   * @return
   */
  def getAddedFields(metsRecordXML: Elem, doi: String): String = {
    var textToAdd = ""

    textToAdd = textToAdd.concat(getDigitalPlatform(metsRecordXML) match {
      case Some(value) =>  "<field name=\"digital_platform_str_mv\">" + scala.xml.Text(value).toString + "</field>"
      case None => ""
    } )

    textToAdd = textToAdd.concat(getIdDigitalPlatform(metsRecordXML) match {
      case Some(value) =>  "<field name=\"id_digital_platform_str_mv\">" + scala.xml.Text(value).toString + "</field>"
      case None => ""
    } )

    textToAdd = textToAdd.concat(buildIiifManifestUrl(getDigitalPlatform(metsRecordXML).getOrElse(""), getIdDigitalPlatform(metsRecordXML).getOrElse("")) match {
      case Some(value) =>  "<field name=\"iiif_manifest_url_str_mv\">" + scala.xml.Text(value).toString + "</field>"
      case None => ""
    } )

    textToAdd = textToAdd.concat(getDigitalReproductionLicence(metsRecordXML) match {
      case Some(value) =>  "<field name=\"digital_reproduction_licence_str_mv\">" + scala.xml.Text(value).toString + "</field>"
      case None => ""
    } )

    textToAdd = textToAdd.concat(getDigitalReproductionRightOwner(metsRecordXML) match {
      case Some(value) =>  "<field name=\"digital_reproduction_right_owner_str_mv\">" + scala.xml.Text(value).toString + "</field>"
      case None => ""
    } )

    textToAdd = textToAdd.concat(getDigitalReproductionCallNumber(metsRecordXML) match {
      case Some(value) =>  "<field name=\"digital_reproduction_call_number_str_mv\">" + scala.xml.Text(value).toString + "</field>"
      case None => ""
    } )

    textToAdd = textToAdd
      .concat(getDigitalObjectPagesDetails(metsRecordXML).flatMap(
        "<field name=\"digital_object_pages_details_str_mv\">" +
          _
          + "</field>"
        )
      )
    textToAdd
  }

  /**
   * Returns the first doi which starts with 10.3931 or 10.7891 in a Some(). Otherwise None
   * This is based on the solr record
   * @param recordXML
   * @return
   */
  def getEplatformDoi(solrRecordXML: Elem): Option[String] = {
      val doi_isn = for {
        field <- solrRecordXML \\ "field"
        if field \@ "name" == "doi_isn_mv"
        if field.text.contains("10.3931") | field.text.contains("10.7891")
      } yield field.text
      doi_isn.headOption
  }

  /**
   * Get the digital platform from METS Record
   * @param metsRecordXML
   * @return "e-rara.ch" or "e-manuscripta.ch"
   */
  def getDigitalPlatform(metsRecordXML: Elem): Option[String] = {
    val oaiIdentifier = (metsRecordXML \ "header" \ "identifier").head.text
    val platformRegex: Regex = """oai:www\.(e-rara\.ch|e-manuscripta\.ch):\d+$""".r

    oaiIdentifier match {
      case platformRegex(platform) => Some(platform)
      case _ => None
    }
  }

  /**
   * Get the id of the digital platform (VLID or Visual Library ID)
   *
   * @param metsRecordXML
   * @return
   */
  def getIdDigitalPlatform(metsRecordXML: Elem): Option[String] = {
    val oaiIdentifier = (metsRecordXML \ "header" \ "identifier").head.text
    val identifierRegex: Regex = """oai:www\.e-(?:rara|manuscripta)\.ch:(\d+)$""".r

    oaiIdentifier match {
      case identifierRegex(identifier) => Some(identifier)
      case _ => None
    }
  }

  /**
   * Build iiif manifest url based on platform and identifier
   * @param platform
   * @param identifier
   * @return
   */
  def buildIiifManifestUrl(platform: String, identifier: String): Option[String] = {
    platform match {
      case "e-rara.ch" => Option("https://www.e-rara.ch/i3f/v20/" + identifier + "/manifest")
      case "e-manuscripta.ch" => Option("https://www.e-manuscripta.ch/i3f/v20/" + identifier + "/manifest")
      case _ => None
    }
  }

  /**
   * From METS: mets:amdSec/mets:rightsMD/mets:mdWrap[@OTHERMDTYPE=”DVRIGHTS”]/mets:xmlData/dv:rights/dv:license
   * @param metsRecordXML
   * @return
   */
  def getDigitalReproductionLicence(metsRecordXML: Elem): Option[String] = {
    val licence = (metsRecordXML \ "metadata" \ "mets" \ "amdSec" \ "rightsMD" \ "mdWrap")
      .filter(node => (node \ "@OTHERMDTYPE").text == "DVRIGHTS")
      .flatMap(_ \ "xmlData" \ "rights" \ "license").head.text
    licence match {
      case value if value.nonEmpty => Some(value)
      case _ => None
    }
  }

  /**
   * From METS, get digital reproduction right owner
   * @param metsRecordXML
   * @return
   */
  def getDigitalReproductionRightOwner(metsRecordXML: Elem): Option[String] = {
    val rightOwner = (metsRecordXML \ "metadata" \ "mets" \ "amdSec" \ "rightsMD" \ "mdWrap")
      .filter(node => (node \ "@OTHERMDTYPE").text == "DVRIGHTS")
      .flatMap(_ \ "xmlData" \ "rights" \ "owner").head.text
    rightOwner match {
      case value if value.nonEmpty => Some(value)
      case _ => None
    }
  }

  /**
   * Aus METS:
   * Wenn vorhanden, dann aus mods:mods/mods:location/mods:shelfLocator
   * Wenn nicht, dann aus mods:mods/mods:location/mods:holdingSimple/mods:copyInformation/mods:shelfLocator
   * @param metsRecordXML
   * @return
   */
  def getDigitalReproductionCallNumber(metsRecordXML: Elem): Option[String] = {
    val callNumber = (metsRecordXML \ "metadata" \ "mets" \ "dmdSec" \ "mdWrap" \ "xmlData" \ "mods" \ "location" \ "shelfLocator").text

    val callNumber2 = (metsRecordXML \ "metadata" \ "mets" \ "dmdSec" \ "mdWrap" \ "xmlData" \ "mods" \ "location" \ "holdingSimple" \ "copyInformation" \ "shelfLocator").text

    callNumber match {
      case value if value.nonEmpty => Some(value)
      case _ =>
        callNumber2 match {
          case value if value.nonEmpty => Some(value)
          case _ => None
        }
    }
  }

  /**
   * For each page of the document, get the details of each page
   * @param metsRecordXML
   * @return
   */
  def getDigitalObjectPagesDetails(metsRecordXML: Elem): Seq[String] = {
    val availableFulltexts = getListOfAvailableFulltexts(metsRecordXML)
    val pageDetails: NodeSeq = (metsRecordXML \ "metadata" \ "mets" \ "structMap")
      .filter(node => (node \ "@TYPE").text == "PHYSICAL") \ "div" \"div"
    pageDetails.map(extractPageInformation(_, availableFulltexts))

  }

  /**
   * For a given page, get a string like this
   * "2940906###Scan 2###1###0" //ID, LABEL, ORDER, Fulltext availability aus METS separated with ###
   * @param page XML Node representing the page
   * @param availableFulltexts List of page identifiers where the fulltext is available
   * @return a string like this "2940906###Scan 2###1###0" //ID, LABEL, ORDER, Fulltext availability aus METS separated with ###
   */
  def extractPageInformation(page: Node, availableFulltexts: Seq[String]): String = {
    val pageId = (page \ "@ID").text.replaceFirst("phys", "")
    val fulltextAvailability = availableFulltexts.contains(pageId)
    val pageInfo = Array(
      pageId,
      scala.xml.Text((page \ "@LABEL").text).toString, //escape the & and other characters which might be in page label
      (page \ "@ORDER").text,
      if (fulltextAvailability) "1" else "0"
    )
    //add fulltext availability
    pageInfo.mkString("###")
  }

  /**
   * Get the page identifiers of all the pages which have a fulltext available
   * (in ALTO format, but this is the base for all fulltext formats)
   * @param metsRecordXML
   * @return
   */
  def getListOfAvailableFulltexts(metsRecordXML: Elem): Seq[String] = {
    val fulltexts = (metsRecordXML \ "metadata" \ "mets" \ "fileSec" \ "fileGrp")
      .filter(node => (node \ "@USE").text == "FULLTEXT") \ "file"
    fulltexts.map(getFulltextId).distinct
  }


  /**
   * To get the fulltext id corresponding to the page id, we need to remove the "ALTO" or "TR" or "HTML" prefixes
   * @param fulltext
   * @return
   */
  def getFulltextId(fulltext: Node): String = {
    (fulltext \ "@ID").text.replace("ALTO","").replace("TR", "").replace("HTML", "")
  }

  /**
   * Get the mongo client for a given database
   * @return
   */
  def getMongoClient: MongoWrapper = {
    val settings = new SettingsLoader(List(
      "mongoDb",
      "mongoUri"
    ).asJava,
      "app.yml", //or local.yml to load local settings
      false,
      false,
      false,
      false)
    MongoWrapper(settings)
  }
}
