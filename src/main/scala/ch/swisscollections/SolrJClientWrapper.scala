/*
 * Hierarchy Enricher
 * Copyright (C) 2020  UB Basel
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */


package ch.swisscollections

import org.apache.logging.log4j.scala.Logging
import org.apache.solr.client.solrj.{SolrClient, SolrQuery}
import org.apache.solr.client.solrj.impl.{CloudSolrClient, HttpSolrClient}
import org.apache.solr.client.solrj.response.UpdateResponse
import org.apache.solr.common.{SolrDocument, SolrInputDocument}

import scala.jdk.CollectionConverters._
import scala.jdk.OptionConverters._
import scala.collection.mutable.ArrayBuffer
import scala.xml.{NodeSeq, XML}
import scala.jdk.CollectionConverters._


class SolrJClientWrapper private (client:SolrClient,
                                  collection:String,
                                 commitBufferLength:Int,
                                  timeDurationForNextSolrCommit: Int) extends Logging {

  var buffer = new ArrayBuffer[SolrInputDocument]()



  def searchBySolrId(solrId:String): List[SolrDocument] = {
    if (solrId.startsWith("(") || solrId == "") {
      return List[SolrDocument]()
    }

    val solrQuery = new SolrQuery
    solrQuery.set("q", "id:" + solrId)
    val res: List[SolrDocument] = client.query(collection, solrQuery).getResults.asScala.toList
    res
  }

  /**
   * Search using old identifiers (for example HAN) from 035$a
   * Will also search using IZ ID. For example if 830$w is 9972407163605504, this will find
   * the parent id
   *
   * @param oldId
   * @return
   */
  def searchByOtherIdentifier(oldId:String): List[SolrDocument] = {
    //remove parenthesis from id
    val cleanId=oldId.replace("(","").replace(")","")
    val solrQuery = new SolrQuery
    solrQuery.set("q", "ctrlnum:" + cleanId)
    val res: List[SolrDocument] = client.query(collection, solrQuery).getResults.asScala.toList
    res
  }

  /**
   * Check if there are records in Solr where the hierarchy_parent_id has the value of the current
   * record
   *
   * @param recordId
   * @return
   */
  def hasChildren(recordId:String): Boolean = {
    val solrQuery = new SolrQuery
    solrQuery.set("q", "hierarchy_parent_id:" + recordId)
    val res: List[SolrDocument] = client.query(collection, solrQuery).getResults.asScala.toList
    res.nonEmpty
  }
}


object SolrJClientWrapper {
  def apply(mode: String, zkHosts: String, collection: String,
            connectionTimeout: String, socketTimeOut: String,
            zkChRoot: Option[String], nodeURL: String,
           solrCommitBufferlength:String,
            timeDurationForNextSolrCommit: String): SolrJClientWrapper = {

    if (mode == "cloud") {

      val client: SolrClient = new CloudSolrClient.Builder(zkHosts.split(",").
        toList.asJava,zkChRoot.toJava)
        .withConnectionTimeout(connectionTimeout.toInt)
        .withSocketTimeout(socketTimeOut.toInt)
        .build()

      new SolrJClientWrapper(client, collection,
        solrCommitBufferlength.toInt, timeDurationForNextSolrCommit.toInt)

    } else {

      val client = new HttpSolrClient.Builder(nodeURL)
        .withConnectionTimeout(connectionTimeout.toInt)
        .withSocketTimeout(socketTimeOut.toInt)
        .build()
      new SolrJClientWrapper(client,collection,
        solrCommitBufferlength.toInt, timeDurationForNextSolrCommit.toInt)
    }
  }

}

sealed trait SolrClientResult extends Product
case object Committed extends SolrClientResult
case object Continue extends SolrClientResult
