/*
 * enricher
 * Copyright (C) 2020  UB Basel
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package ch.swisscollections

import ch.swisscollections.EnrichFromMongo.enrichWithERaraEManuscriptaInfo
import org.apache.kafka.streams.scala.kstream.KStream
import org.apache.kafka.streams.{KeyValue, Topology}
import org.apache.kafka.streams.scala.ImplicitConversions._
import org.apache.kafka.streams.scala.{StreamsBuilder, _}
import org.apache.logging.log4j.scala.Logging

class KafkaTopology extends Logging {

  import KafkaTopologyUtils._
  import Serdes._


  def build(): Topology = {
    val builder = new StreamsBuilder

    val messages: KStream[String, String] = builder.stream[String, String](SettingsFromFile.getKafkaInputTopic)

    messages
      .mapValues(
        message => hierarchyEnriched(message)
      ).mapValues(
        message => enrichWithERaraEManuscriptaInfo(message)
      ).to(SettingsFromFile.getKafkaOutputTopic)

    builder.build()
  }

}
