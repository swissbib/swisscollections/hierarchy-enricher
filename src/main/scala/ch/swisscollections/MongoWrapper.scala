/*
 * enricher
 * Copyright (C) 2020  UB Basel
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package ch.swisscollections

import com.mongodb.{BasicDBObject, MongoClient, MongoClientURI, MongoException}
import com.mongodb.client.{MongoCollection, MongoDatabase}
import org.bson.Document
import ch.memobase.settings.SettingsLoader
import org.bson.conversions.Bson
import org.apache.logging.log4j.scala.Logging

import java.io.ByteArrayOutputStream
import java.util.zip.Inflater
import scala.collection.JavaConverters.iterableAsScalaIterableConverter

class MongoWrapper(val database: MongoDatabase) extends Logging {

  /**
   * Get the deserializeed xml record as string from the record field
   * of a document with the corresponding doi
   * It checks the prefix to determine in which mongo collection to search
   * (could be erara or emanuscripta)
   *
   * @param doi
   * @return
   */
  def findByDoi(doi:String): Option[String] = {
    val bdbo = new BasicDBObject()
    bdbo.put("doi",  doi)
    val collection = doi.take(7) match {
      case "10.3931" => "erara"
      case "10.7891" => "emanuscripta"
      case _ => "erara" //default to erara
    }
    val mongoCollection = database.getCollection(collection)
    try {
      val record = mongoCollection.find(bdbo).asScala.head.get("record").asInstanceOf[org.bson.types.Binary]
      Some(deserialize(record.getData))
    } catch {
      case e: NoSuchElementException =>
        logger.info(doi + " not found in mongo")
        None
      case e: MongoException =>
        logger.error("Mongo error :" + e.getMessage)
        None
      case _: Throwable =>
        logger.error("Problem retrieving record from mongo with doi " + doi)
        None
    }
  }

  def deserialize(data: Array[Byte]): String = {
    val decompressor = new Inflater()
    decompressor.setInput(data)
    val bos = new ByteArrayOutputStream(data.length)
    val buffer = new Array[Byte](8192)
    while (!decompressor.finished) {

      val size = decompressor.inflate(buffer)
      bos.write(buffer, 0, size)
    }
    bos.toString


  }

}

object MongoWrapper {

  def apply (settings: SettingsLoader): MongoWrapper = {

    val uri = new MongoClientURI(settings.getAppSettings.getProperty("mongoUri"))
    val mongoClient = new MongoClient(uri)

    val database = mongoClient.getDatabase(settings.getAppSettings.getProperty("mongoDb"))

    new MongoWrapper(database)
  }

}
